import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * HTTP / URL SERVER
 * This class defines the HTTP Server where content will be retrieved.
 *
 * @author Eduardo Rodrigues
 * @version 2.0
 * @since 2020-12-02
 */
public class HttpServer {

    private String httpServerUrl;

    // Set the default messages and values
    private static final String PROCESSING_ERROR_MSG = "There was an error while processing the content.";
    private static final String GENERAL_ERROR_MSG = "Something went wrong!";

    /**
     * Non-Default Constructor
     *
     * @param url of the HTTP server
     */
    public HttpServer(String url) {
        this.httpServerUrl = url;
    }

    /**
     * This function gets contents from the remote (Web) or local (Http Server).
     *
     * @param endPointAddress is the endpoint where data must be pulled
     * @param filter          to be applied
     * @return Json Content List
     */
    public List<String> getJsonContent(String endPointAddress, String filter) {

        String jsonString = null;
        List<String> jsonList = new ArrayList<>();

        try {
            // Prepare the connection
            URL url = new URL(this.httpServerUrl + endPointAddress + filter);
            URLConnection conn = url.openConnection();
            // Result: the connection is opened to the target (httpServerUrl)

            // Retrieve the content from the server
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((jsonString = reader.readLine()) != null) {
                jsonList.add(jsonString);
            }
            // Result: Since the contents are based on API, JSON format will be retrieved from the server and stored in a list.

        } catch (JsonProcessingException ex) {
            System.out.println(PROCESSING_ERROR_MSG);   // If there is an error while processing the JSON.
        } catch (Exception ex) {
            System.out.println(GENERAL_ERROR_MSG);      // If a general error is identified.
        }

        return jsonList;  // Result: returns a list of JSON content.
    }
}