import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * CLIENT TIER
 * This application pull data from the Servlet Server and display on the console.
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-12-04
 *
 * ********************************************
 * ********** INCLUDED REQUIREMENTS ***********
 * ********************************************
 * ON THIS CLIENT TIER:
 *
 * *** Java Collections;
 * *** Exception Handling and Data Validation;
 * *** JSON and HTTP/URL;
 *
 * ********************************************
 *  ON THE SERVER TIER:
 *
 *  *** Java Collections;
 *  *** Exception Handling and Data Validation;
 *  *** JSON;
 *  *** Hibernate;
 *  *** Servlet
 *
 *  ********************************************
 *  ADDITIONAL REQUIREMENTS ON THE PROJECT:
 *
 * *** Use Case Document
 * *** UML Sequence Diagram
 *
 * ********************************************
 */
public class Main {

    // Set the default messages and values
    private static final String HTTP_SERVER_URL = "http://localhost:8080/APIServer/";
    private static final String END_POINT_POSITION = "searchbyposition?position=";
    private static final String END_POINT_NAME = "searchbyname?name=";
    private static final String END_POINT_LIST = "searchbyname";
    private static final String WHAT_PROGRAM_DOES_MSG = "\nThis program presents a short biography of each prophet of the current dispensation.";
    private static final String WHAT_LIKE_TODO_MSG = "\nWhat would you like to do?";
    private static final String FIND_PROPHET_BY_POSITION_MSG = "\n   1. Find a prophet by the position of his calling;";
    private static final String FIND_PROPHET_BY_NAME_MSG = "\n   2. Find a prophet by name;";
    private static final String LIST_PROPHETS_MSG = "\n   3. See a list of all prophets of this dispensation;";
    private static final String CLOSE_MSG = "\n   4. Close the application.";
    private static final String INSERT_OPTION_MSG = "\nYour option: ";
    private static final String INVALID_INTEGER_MSG = "Please, only numbers are accepted!\n";
    private static final String INVALID_INPUT_MSG = "Invalid input!\n";
    private static final String WHICH_POSITION_MSG = "Which prophet position (from 1 to 17): ";
    private static final String INSERT_A_POSITION_MSG = "\nInsert a valid prophet position!";
    private static final String WHICH_NAME_MSG = "Which prophet name: ";
    private static final String INSERT_A_NAME_MSG = "\nInsert a valid prophet name!";
    private static final String PRESS_ENTER_MSG = "Press any ENTER to continue!";
    private static final String NO_RESULT_MSG = "No result to be displayed based on your choices!";
    private static final String ERROR_PARSING_MSG = "\nThere was a problem while reading data. Please, try again later or check or input.";
    private static final String ERROR_READING_MSG = "\nThere was a problem while reading the content received from the server.";
    private static final String GOODBYE_MSG = "\nGoodbye ...";

    // Scanner input
    private static Scanner sc = new Scanner(System.in);

    /**
     * This function gets an option from the user to be executed in the program.
     *
     * @return the option chosen by the user.
     */
    private static int getOptionFromUser() {

        int optionInput = -1;

        do {
            // Output the available options of the program
            System.out.print(WHAT_LIKE_TODO_MSG);
            System.out.print(FIND_PROPHET_BY_POSITION_MSG);
            System.out.print(FIND_PROPHET_BY_NAME_MSG);
            System.out.print(LIST_PROPHETS_MSG);
            System.out.println(CLOSE_MSG);
            System.out.print(INSERT_OPTION_MSG);

            // Check if the input is valid
            try {
                optionInput = sc.nextInt();
            } catch (InputMismatchException ex) {
                sc.next();
                System.out.print(INVALID_INTEGER_MSG);       // Result: friendly user message -> Data Validation
            } finally {
                sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])");  // This will skip spurious as well as carriage return.
            }
            // If the user does type a number out of menu option range, it will keep asking the user for an option.
        } while (optionInput < 1 || optionInput > 4);

        return optionInput; // Result: Option menu properly chosen by the user and validated.
    }

    /**
     * This function gets the parameters from the user to be executed in the program.
     *
     * @return the parameter (Prophet's name or Prophet's calling sequence) from the user.
     */
    private static String getParameterFromUser(String message, String errorMsg) {
        String parameter = "";

        do {
            // Output the message to instruct the user
            System.out.print(message);

            // Check if the input is valid
            try {
                parameter = sc.next();
            } catch (InputMismatchException ex) {
                sc.next();
                System.out.print(INVALID_INPUT_MSG);         // Result: friendly user message
            } finally {
                sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])");  // This will skip spurious as well as carriage return.
            }
            // If the user does not enter a parameter, the app will keep asking for one.
            if (parameter.equals(""))
                System.out.println(errorMsg);

        } while (parameter.equals(""));

        return parameter;   // Result: Option menu properly chosen by the user and validated.
    }

    /**
     * This function converts a JSON object to Prophet object.
     *
     * @param jsonList object
     * @return a list of Prophets objects
     */
    private static List<Prophet> ConvertJSONToProphet(List<String> jsonList) {

        // Object to map the object to Json format.
        ObjectMapper mapper = new ObjectMapper();
        List<Prophet> prophetList = new ArrayList<>();

        // Start the conversion from JSON to Prophet object and add to a list
        try {
            for (String json : jsonList) {
                prophetList.add(mapper.readValue(json, Prophet.class)); // Result: Json string formatted to Object.
            }
        } catch (JsonProcessingException e) {
            System.out.println(ERROR_PARSING_MSG);      // Result: Friendly user error message;
        } catch (IOException e) {
            System.out.println(ERROR_READING_MSG);      // Result: Friendly user error message;
        }

        return prophetList; // Result: Object Prophet list is returned.
    }

    /**
     * This function displays all the prophets on the screen.
     *
     * @param prophetList is a list with all prophets objects.
     */
    public static void displayProphetList(List<Prophet> prophetList) {

        // Check if the list is not empty, otherwise, the list will be displayed.
        if (prophetList.size() != 0) {
            for (Prophet prophet : prophetList) {
                System.out.println(prophet.toString());
            }
            // Result: All prophets are displayed on the screen.
        } else {
            System.out.println(NO_RESULT_MSG);      // Result: Friendly user error message;
        }

        System.out.println(PRESS_ENTER_MSG);        // Result: Press any key to continue.
        sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?"); // This will skip spurious as well as carriage return.
    }

    /**
     * This method defines the entry point for the program.
     *
     * @param args to be passed to the main() method
     */
    public static void main(String[] args) {

        // Create an instance of the HTTP Server
        HttpServer server = new HttpServer(HTTP_SERVER_URL);

        // Display the header of the program
        System.out.println(WHAT_PROGRAM_DOES_MSG);

        int option = 4;
        // Show Menu and Get Option From User
        do {
            option = getOptionFromUser();   //Result: option chosen by the user.
            switch (option) {
                case 1:
                    // Find prophet by position in the restoration.
                    String position = getParameterFromUser(WHICH_POSITION_MSG, INSERT_A_POSITION_MSG);
                    List<String> jsonFromPositionList = server.getJsonContent(END_POINT_POSITION, position);
                    List<Prophet> prophetsFromPosition = ConvertJSONToProphet(jsonFromPositionList);
                    displayProphetList(prophetsFromPosition);
                    // Result: a prophet will be displayed according to his calling position.
                    break;
                case 2:
                    // Find prophet by name.
                    String name = getParameterFromUser(WHICH_NAME_MSG, INSERT_A_NAME_MSG);
                    List<String> jsonFromNameList = server.getJsonContent(END_POINT_NAME, name);
                    List<Prophet> prophetsFromName = ConvertJSONToProphet(jsonFromNameList);
                    displayProphetList(prophetsFromName);
                    // Result: a list of prophets will be displayed based on their/his name(s).
                    break;
                case 3:
                    // Display all prophets
                    List<String> jsonFromList = server.getJsonContent(END_POINT_LIST, "");
                    List<Prophet> prophetsFromList = ConvertJSONToProphet(jsonFromList);
                    displayProphetList(prophetsFromList);
                    // Result: a list of all prophets will be displayed.
                    break;
            }
        } while (option != 4); // Only four options are available in this program.

        System.out.println(GOODBYE_MSG);    // Display Goodbye message
    }
}