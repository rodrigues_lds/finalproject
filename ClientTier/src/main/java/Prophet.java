import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

/**
 * PROPHET CLASS
 * This class defines the entity of the Prophet object.
 *
 * @author Eduardo Rodrigues
 * @version 2.0
 * @since 2020-11-07
 *
 */

// The following tag ignore data from JSON object that is not mapped in this class.
@JsonIgnoreProperties(ignoreUnknown = true)
public class Prophet implements Serializable {

    /**
     * Default constructor
     */
    public Prophet() {
        this.id = 0;
        this.name = "";
        this.position = 0;
        this.origin = "";
        this.profession = "";
        this.born = 0;
        this.sustained = 0;
        this.deathAge = 0;
        this.presidentLength = 0;
        this.summary = "";
    }

    /**
     * Non-Default constructor
     *
     * @param id                of the prophet
     * @param name              prophet given name
     * @param position          when called to be a prophet
     * @param origin            place where the prophet was born
     * @param profession        before being called to the apostleship
     * @param born              year when the prophet was born
     * @param sustained         year when the prophet was sustained to be a prophet
     * @param deathAge          age when the prophet died
     * @param presidentLength   time the prophet stayed as president of the church
     * @param summary           summary of the prophet's life
     */
    public Prophet(int id, String name, int position, String origin, String profession, int born, int sustained,
                   int deathAge, int presidentLength, String summary) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.origin = origin;
        this.profession = profession;
        this.born = born;
        this.sustained = sustained;
        this.deathAge = deathAge;
        this.presidentLength = presidentLength;
        this.summary = summary;
    }

    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    private int position;
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }

    private String origin;
    public String getOrigin() {
        return origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    private String profession;
    public String getProfession() {
        return profession;
    }
    public void setProfession(String profession) {
        this.profession = profession;
    }

    private int born;
    public int getBorn() {
        return born;
    }
    public void setBorn(int born) {
        this.born = born;
    }

    private int sustained;
    public int getSustained() {
        return sustained;
    }
    public void setSustained(int sustained) {
        this.sustained = sustained;
    }

    private int deathAge;
    public int getDeathAge() {
        return deathAge;
    }
    public void setDeathAge(int deathAge) {
        this.deathAge = deathAge;
    }

    private int presidentLength;
    public int getPresidentLength() {
        return presidentLength;
    }
    public void setPresidentLength(int presidentLength) {
        this.presidentLength = presidentLength;
    }

    private String summary;
    public String getSummary() {
        return summary;
    }
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * This function returns the prophet string format.
     *
     * @return prophet string format.
     */
    public String toString(){
        return  "\n" +
                "Position: " + this.position + "\n" +
                "Name: " + this.name + "\n" +
                "Origin: " + this.origin + "\n" +
                "Profession: " + this.profession + "\n" +
                "Born year: " + this.born + "\n" +
                "Sustained as a prophet on: " + this.sustained + "\n" +
                "Death Age: " + this.deathAge + "\n" +
                "Presided the church for: " + this.presidentLength + " years." + "\n" +
                "Biography: " + this.summary + "\n";
    }
}