import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * ENDPOINT - SEARCH BY PROPHET POSITION
 *
 * @author Eduardo Rodrigues
 * @version 1.0
 * @since 2020-12-05
 * <p>
 * This class provides a response from the server, specific for endpoint to search a prophet by name.
 * The servlet process the request by the parameter 'name'.
 * The servlet is running in a local server (Tomcat).
 *
 * ********************************************
 * ********** INCLUDED REQUIREMENTS ***********
 * ********************************************
 * ON THE CLIENT TIER:
 *
 * *** Java Collections;
 * *** Exception Handling and Data Validation;
 * *** JSON and HTTP/URL;
 *
 * ********************************************
 *  ON THIS SERVER TIER:
 *
 *  *** Java Collections;
 *  *** Exception Handling and Data Validation;
 *  *** JSON;
 *  *** Hibernate;
 *  *** Servlet
 *
 *  ********************************************
 *  ADDITIONAL REQUIREMENTS ON THE PROJECT:
 *
 * *** Use Case Document
 * *** UML Sequence Diagram
 *
 * ********************************************
 */

@WebServlet(urlPatterns = "/searchbyname")
public class EndPointName extends HttpServlet {

    // Set the default messages and values
    private static final String SOMETHING_WENT_WRONG_MSG = "Something went wrong. Try again later.";
    private static final String ERROR_WHILE_READING_MSG = "It was not possible to read content from the server. Check if the server is on!";
    private static final String PARAMETER_BAD_FORMAT_MSG = "Wrong data sent to the server. Check your parameters!";
    private static final String NO_CONTENT_TO_DISPLAY_MSG = "No content to display. Perform a search by using name as parameter.";
    private static final String PERSISTENCE_UNIT_NAME = "org.hibernate.tutorial.jpa";
    private static final String PARAMETER = "name";

    /**
     * This function retrieves a prophet from the database by the name.
     *
     * @param name of the prophet.
     * @return a prophet object.
     */
    public static List<Prophet> findProphetByName(String name) {
        // This interface is used to interact with the entity manager factory for the persistence unit.
        // the org.hibernate.tutorial.jpa was defined in the persistence.xml file.
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        // Entity Manager is used to create and remove persistent entity instances to find entities by their key or to query over entities.
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        List<Prophet> prophets = new ArrayList<>();

        // Pull data from the database
        try {
            entityManager.getTransaction().begin(); // It begins the transaction/connection with the database.
            prophets = entityManager.createNativeQuery("SELECT * FROM TBPROPHET WHERE NAME LIKE \'%" + name + "%\'", Prophet.class).getResultList();
        } catch (IllegalStateException ex) {
            System.out.println(SOMETHING_WENT_WRONG_MSG); // Result: Friendly user error message;
        } finally {
            entityManagerFactory.close();   // It closes the Manager Factory instance.
            entityManager.close();          // It closes the Manager instance.
        }

        return prophets; // Result: A prophet list is retrieved from the database according to its id (position calling in this dispensation)
    }

    /**
     * This function process the request sent from the client.
     *
     * @param req is the request sent by the client.
     * @param res is the response to be provided.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) {

        PrintWriter writer = null;

        try {
            // Get parameter from endpoint
            String filter = (req.getParameter(PARAMETER) == null) ? "" : req.getParameter(PARAMETER);
            writer = res.getWriter();
            // Page structure, by pulling data from the database
            List<Prophet> prophets = findProphetByName(filter);
            for (Prophet prophet : prophets) {
                writer.println(prophet.toJSON());
            }
            // Result: content published in the endpoint by using Json format
        } catch (IOException ex) {
            writer.println(ERROR_WHILE_READING_MSG);        // Result: Friendly user error message;
        } catch (NumberFormatException ex) {
            writer.println(PARAMETER_BAD_FORMAT_MSG);       // Result: Friendly user error message;
        } catch (Exception ex) {
            writer.println(NO_CONTENT_TO_DISPLAY_MSG);      // Result: Friendly user error message;
        }
        // Result: A list of prophets with the specified name will be populated in JSON format.
    }
}